import dash
from dash import dcc, html
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import pandas as pd
from typing import List, Tuple
import geopandas as gpd
from shapely.geometry import Polygon

def create_expanded_grid_corners(tl: Tuple[float, float], tr: Tuple[float, float],
                                 bl: Tuple[float, float], br: Tuple[float, float],
                                 res_x: int, res_y: int) -> List[List[Tuple[float, float]]]:
    """
    Generate a grid of corner points (res_x + 1) x (res_y + 1) evenly distributed in the rectangle.
    The given corners (tl, tr, bl, br) are in the center of the edge cells.
    """

    # Calculate the vectors
    top_vec = np.array(tr) - np.array(tl)
    left_vec = np.array(bl) - np.array(tl)
    right_vec = np.array(br) - np.array(tr)
    bottom_vec = np.array(br) - np.array(bl)

    # Calculate the expanded corners
    expanded_tl = np.array(tl) - top_vec / (2 * res_x) - left_vec / (2 * res_y)
    expanded_tr = np.array(tr) + top_vec / (2 * res_x) - right_vec / (2 * res_y)
    expanded_bl = np.array(bl) - bottom_vec / (2 * res_x) + left_vec / (2 * res_y)
    expanded_br = np.array(br) + bottom_vec / (2 * res_x) + right_vec / (2 * res_y)

    # Generate the grid corners
    grid_corners = []
    for i in range(res_y + 1):
        row = []
        for j in range(res_x + 1):
            point = (expanded_tl * (res_y - i) * (res_x - j) +
                     expanded_tr * (res_y - i) * j +
                     expanded_bl * i * (res_x - j) +
                     expanded_br * i * j) / (res_x * res_y)
            row.append((point[0], point[1]))
        grid_corners.append(row)

    return grid_corners

def create_grid_cells(corners: List[List[Tuple[float, float]]]) -> List[Polygon]:
    """
    Create grid cells from grid corners.
    """

    cells = []
    for i in range(len(corners) - 1):
        for j in range(len(corners[i]) - 1):
            cell = Polygon([
                (corners[i][j][1], corners[i][j][0]),
                (corners[i][j + 1][1], corners[i][j + 1][0]),
                (corners[i + 1][j + 1][1], corners[i + 1][j + 1][0]),
                (corners[i + 1][j][1], corners[i + 1][j][0])
            ])
            cells.append(cell)
    
    return cells

# Define the corners of the rectangle
tl = (50.33422, 6.94366)
tr = (50.33549, 6.9456)
bl = (50.33392, 6.94413)
br = (50.33517, 6.94606)

# Define the resolution of the grid
res_x = 19  # Number of cells in the x direction
res_y = 5   # Number of cells in the y direction

# Generate the expanded grid corners
grid_corners = create_expanded_grid_corners(tl, tr, bl, br, res_x, res_y)

# Generate the grid cells
grid_cells = create_grid_cells(grid_corners)

# Load data from the file
data_df = pd.read_csv('longarray.txt', header=None)

# Create a GeoDataFrame with the grid cells (initially with the first row of data)
gdf = gpd.GeoDataFrame({'geometry': grid_cells, 'value': data_df.iloc[0].tolist()})

# Initialize the Dash app
app = dash.Dash(__name__)

# Create the initial figure using Plotly Express Choropleth map
fig = px.choropleth_mapbox(
    gdf,
    geojson=gdf.geometry,
    locations=gdf.index,
    color='value',
    color_continuous_scale="Reds",  # Set colormap to Reds
    range_color=(0, 50),  # Fix the color range
    mapbox_style="carto-positron",
    center={"lat": 50.3345, "lon": 6.9448},
    zoom=17,
    opacity=0.5,
    height=1000
)

# Add text annotations for each cell
def add_text_annotations(fig, grid_cells, data):
    for i, cell in enumerate(grid_cells):
        centroid = cell.centroid
        fig.add_trace(go.Scattermapbox(
            lat=[centroid.y],
            lon=[centroid.x],
            mode='text',
            text=[f'{data[i]:.0f}'],
            textfont=dict(color='black', size=12),
            showlegend=False
        ))

add_text_annotations(fig, grid_cells, data_df.iloc[0].tolist())

# Update layout for rotation and full screen
fig.update_layout(
    mapbox=dict(
        bearing=-45,  # Rotate the map by -45 degrees
        pitch=0
    ),
    margin={"r":0,"t":0,"l":0,"b":0},  # Set margins to zero for full screen
    autosize=True,
    coloraxis_showscale=False  # Hide the color scale legend
)

# Define the layout of the Dash app
app.layout = html.Div([
    dcc.Graph(id='choropleth', figure=fig, style={'height': '100vh', 'width': '100vw'}),
    dcc.Interval(
        id='interval-component',
        interval=2.5*1000,  # Update every 2 seconds
        n_intervals=0
    )
], style={'height': '100vh', 'width': '100%', 'overflow': 'hidden'})

# Update the figure at regular intervals
@app.callback(
    dash.dependencies.Output('choropleth', 'figure'),
    [dash.dependencies.Input('interval-component', 'n_intervals')],
    [dash.dependencies.State('choropleth', 'relayoutData')]
)
def update_figure(n, relayout_data):
    data = data_df.iloc[n % len(data_df)].tolist()
    gdf['value'] = data
    fig = px.choropleth_mapbox(
        gdf,
        geojson=gdf.geometry,
        locations=gdf.index,
        color='value',
        color_continuous_scale="Reds",  # Set colormap to Reds
        range_color=(0, 50),  # Fix the color range
        mapbox_style="open-street-map",
        center={"lat": 50.3345, "lon": 6.9448},
        zoom=17,
        opacity=0.5,
        height=1000
    )
    add_text_annotations(fig, grid_cells, data)
    
    # Preserve user map interactions (zoom, pan)
    if relayout_data and "mapbox.center" in relayout_data:
        fig.update_layout(
            mapbox=dict(
                center=relayout_data["mapbox.center"],
                zoom=relayout_data["mapbox.zoom"],
                bearing=relayout_data["mapbox.bearing"],
                pitch=relayout_data["mapbox.pitch"]
            )
        )
    
    fig.update_layout(
        mapbox=dict(
            bearing=-45,  # Rotate the map by -45 degrees initially
            pitch=0
        ),
        margin={"r":0,"t":0,"l":0,"b":0},  # Set margins to zero for full screen
        autosize=True,
        coloraxis_showscale=False  # Hide the color scale legend
    )
    return fig

# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)
